import { Home } from "../components/pages/Home";

export const homeRouters = [
  {
    path: '/home',
    element: <Home />
  }
]