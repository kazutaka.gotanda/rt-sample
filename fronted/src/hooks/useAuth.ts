import { useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { postAuthLoginApi } from "../api/apiAuth";



export const useAuth = () => {
  const navigate = useNavigate();

  const login = useCallback(
    async (id: string, password: string) => {
      const data = {
        id: `${id}`,
        password: `${password}`,
      }
      const res = await postAuthLoginApi(data);
      if (res.status !== 200) {
        return alert("ログインに失敗しました");
      }
      navigate("/home");
    },
    []
  );

  return {login}
}