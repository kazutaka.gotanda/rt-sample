import { AxiosResponse } from "axios";
import { UserLoginResponseType, UserRequestType } from "../type/api/user";
import { api } from "./axiosConfig";

export const postAuthLoginApi = (data: UserRequestType) => {
  return api
    .post<UserLoginResponseType>("/admin/auth/login", data)
    .then((res: AxiosResponse) => {
      return res;
    })
    .catch((error) => {
      return error.response;
    });
};