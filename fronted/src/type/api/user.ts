export type UserRequestType = {
  id: string;
  password: string;
};

export type UserLoginResponseType = {
  data: {
    user_name: string;
  };
};